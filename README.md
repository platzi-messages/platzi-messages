# Platzi Messages

<p align="center"><img src="https://s3.us-west-2.amazonaws.com/secure.notion-static.com/03e56d3a-c6d7-4fe0-9cb8-f7c5c2341647/logoplatzimessage.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT73L2G45O3KS52Y5%2F20201018%2Fus-west-2%2Fs3%2Faws4_request&X-Amz-Date=20201018T020755Z&X-Amz-Expires=86400&X-Amz-Signature=c091a3e21bf44bdffcd8c392bbf55044d0a963714750255f6251e62b0ec54c6a&X-Amz-SignedHeaders=host&response-content-disposition=filename%20%3D%22logoplatzimessage.png%22" alt=""></p>

<p align="center">
	<img src="https://img.shields.io/badge/npm-6.14.4-blue"  alt="npm badge">
	<img src="https://img.shields.io/badge/license-GPL--3.0-blue"  alt="license badge">
	<img src="https://img.shields.io/badge/react-16.14.0-blue"  alt="react bagde">
	<img src="https://img.shields.io/badge/version-1.0.0-green"  alt="version bagde">
</p>

Platzi Messages frontend
This project is a webchat to have an alternative to Slack in a collaboration platform.

## How to clone
You can clone the repository

    $ git clone https://gitlab.com/platzi-messages/platzi-messages.git

## Installation
To install this project just type

    $ yarn install

To execute type

    $ yarn start

To build type

    $ yarn build


## Notes

Code should be run using:

- yarn v:^1.22.0
- node v:^10.22.0

## How to contribute

You can create a Merge Request to the project

## License

GPL-3.0