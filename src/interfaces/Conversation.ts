
export interface Threads {
  has_unreads: boolean;
  mention_count: number;
}

export interface Channel {
  id: string;
  has_unreads: boolean;
  mention_count: number;
  latest: string;
  last_read: string;
}

export interface DirectMessage {
  id: string;
  has_unreads: boolean;
  last_read: string;
  mention_count: number;
  latest: string;
}

export interface Conversation {
  ok: boolean;
  threads?: Threads;
  channels?: Channel[];
  directMessages?: DirectMessage[];
  integrations?: any[];
  homeBadgeCount?: number;
}
