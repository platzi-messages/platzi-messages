import styled from 'styled-components'

export const ProfileStyled = styled.div`
  display: grid;
  grid-template-rows: repeat(4, 1fr) 50px;
  align-items: center;
  height: 100%;
`

export const ImageContainer = styled.div`
  position: relative;
  margin-right: 2em;
  width: 40%;
  height: 140px;
  & label {
    position: absolute;
    bottom: 0;
    left: 35%;
    width: 40px;
    height: 40px;
    cursor: pointer;
    color: white;
  }
  & img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
  input[type='file'] {
    display: none;
  }
`

export const UserData = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 45%);
  justify-content: space-between;
`

export const HeaderProfile = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const Description = styled.div`
  align-self: end;
  & p {
    margin: .5em 0;
    color: white;
  }
`

export const Skills = styled.div`
  align-self: center;
`

export const Options = styled.div`
  display: grid;
  grid-template-columns: ${({ isEdit }) => (isEdit ? '60% 20% 20%' : '80% 20%')};
  align-items: center;
  height: 100%;
  color: ${({ theme }) => theme.colors.text_color};
  & button {
    width: 90px;
    height: 40px;
    border-radius: 6px;
    background-color: ${({ theme }) => theme.colors.actionOne_color};
    cursor: pointer;
    color: inherit;
    &:first-of-type {
      background-color: ${({ theme }) => theme.colors.text_color};
      color: black;
    }
  }
`
