import React, { useState } from 'react'
import { MdSync } from 'react-icons/md'
import useInputValue from '../../hooks/useInputValue'
import MainInput from '../MainInput/MainInput'
import { ProfileStyled, HeaderProfile, Description, Skills, Options, ImageContainer, UserData } from './Profile.styled'

export default function Profile() {
  const [edit, setEdit] = useState(false)
  const [image, setImage] = useState(null)
  const [name] = useInputValue('name')
  const [description] = useInputValue('description')
  const [location] = useInputValue('location')
  const [email] = useInputValue('email')
  const [skills] = useInputValue('skills')
  console.log(image)
  return (
    <ProfileStyled>
      <HeaderProfile>
        <ImageContainer>
          <img src="https://orientacion-laboral.infojobs.net/wp-content/uploads/2017/11/9-si-foto-cv.png" alt="User profile" />
          {edit && (
            <label htmlFor="userProfile">
              <MdSync size="40px" />
              <input
                type="file"
                id="userProfile"
                onChange={(e) => setImage(e.target.files[0])}
                accept="image/png, image/jpeg"
              />
            </label>
          )}
        </ImageContainer>
        <MainInput
          title={edit ? 'Change Name' : 'display name'}
          disabled={!edit}
          htmlFor="Name"
          value={name.value}
          onChange={name.onChange}
        />
      </HeaderProfile>
      <Description>
        <MainInput
          title="description"
          disabled={!edit}
          value={description.value}
          onChange={description.onChange}
        />
      </Description>
      <UserData>
        <MainInput
          title="Location"
          disabled={!edit}
          value={location.value}
          onChange={location.onChange}
        />
        <MainInput
          title="Email"
          disabled={!edit}
          value={email.value}
          onChange={email.onChange}
        />
      </UserData>
      <Skills>
        <MainInput
          title="skills"
          disabled={!edit}
          value={skills.value}
          onChange={skills.onChange}
        />
      </Skills>
      <Options isEdit={edit}>
        <p>Leave team</p>
        {edit && <button type="button" onClick={() => setEdit(!edit)}>Cancel</button>}
        <button type="button" onClick={() => setEdit(!edit)}>
          {edit ? 'Save' : 'Edit'}
        </button>
      </Options>
    </ProfileStyled>
  )
}
