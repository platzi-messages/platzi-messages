import React from 'react'
import { MainInputStyled, InputContainer } from './MainInput.styled'

export default function MainInput({ title, value, onChange, type = 'text', disabled, htmlFor }) {
  return (
    <MainInputStyled>
      <label htmlFor={htmlFor}>
        <p>{title}</p>
        <InputContainer>
          <input
            type={type}
            value={value}
            onChange={onChange}
            disabled={disabled}
            id={htmlFor}
          />
        </InputContainer>
      </label>
    </MainInputStyled>
  )
}
