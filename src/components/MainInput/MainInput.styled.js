import styled from 'styled-components'

export const MainInputStyled = styled.div`
  width: 100%;
  color: white;
  & p {
    margin: .5em 0;
    text-transform: capitalize;
    color: ${({ theme }) => theme.colors.text_color};
  }
`

export const InputContainer = styled.div`
  overflow: hidden;
  padding: ${({ theme }) => theme.space}px;
  border: 1px solid ${({ theme }) => theme.colors.border_color};
  border-radius: 5px;
  input {
    outline: none;
    width: 100%;
    height: 100%;
    border: none;
    background-color: inherit;
    font-size: 1em;
    color: ${({ theme }) => theme.colors.text_color};
  }
`
