import styled from 'styled-components'

const SideMenuStyled = styled.aside`
  padding: 1em 0;
  height: 100vh;
  grid-area: side;
  word-break: break-all;
  background: ${(props) => props.theme.colors.primary_color};
  text-align: left;
  overflow-y: scroll;
  white-space: pre;
`

export default SideMenuStyled
