import React from 'react'
import { MdAddCircleOutline } from 'react-icons/md'
import SideMenuOption from '../SideMenuOption'
import SideMenuHeader from '../SideMenuHeader'
import SideMenuTitle from '../SideMenuTitle'
import SideMenuStyled from './SideMenu.styled'

export default function SideMenu({ conversations }) {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  const { channels, direct_messages: directMessages } = conversations

  return (
    <SideMenuStyled>
      <SideMenuHeader />
      <SideMenuTitle
        title="Channels"
        Icon={MdAddCircleOutline}
        onClick={() => alert('click')}
      />
      {channels?.map((channel) => (
        <SideMenuOption
          key={channel.id}
          channel={channel}
        />
      ))}
      <SideMenuTitle
        title="Direct Messages"
        Icon={MdAddCircleOutline}
        onClick={() => alert('click')}
      />
      {
          directMessages?.map((channel) => (
            <SideMenuOption
              isDirect
              key={channel.id}
              channel={channel}
            />
          ))
      }
    </SideMenuStyled>
  )
}
