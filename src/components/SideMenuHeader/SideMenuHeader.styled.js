import styled from 'styled-components'

export const CompanyWrapper = styled.div`
display: flex;
align-items: flex-start;
cursor: pointer;
font-size: 1.2em;
  & h3 {
    font-family: ${(props) => props.theme.fonts.Lato};
  }
`

export const UserWrapper = styled.div`
display: flex;
& p {
  margin-left: .5em;
  font-family: ${(props) => props.theme.fonts.Lato};
  text-transform: capitalize;
}
`

export const SideHeaderStyled = styled.div`
  padding: ${(props) => props.theme.space * 2}px;
  color: ${(props) => props.theme.colors.text_color};
`
