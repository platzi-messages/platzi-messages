import React from 'react'
import { MdFiberManualRecord } from 'react-icons/md'
import { RiArrowDropDownLine } from 'react-icons/ri'
import { SideHeaderStyled, CompanyWrapper, UserWrapper } from './SideMenuHeader.styled'

export default function SideMenuHeader() {
  return (
    <SideHeaderStyled>
      <CompanyWrapper>
        <h3>ExampleCompany</h3>
        <RiArrowDropDownLine size="40px" viewBox="5 2 25 30" />
      </CompanyWrapper>
      <UserWrapper>
        <MdFiberManualRecord color="#33B13A" />
        <p>john taylor</p>
      </UserWrapper>
    </SideHeaderStyled>
  )
}
