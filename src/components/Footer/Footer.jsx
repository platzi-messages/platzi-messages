import React from 'react'
import FooterStyled from './Footer.styled'

export default function Footer() {
  return (
    <FooterStyled>
      <p>Team</p>
      <p>Contact Us</p>
      <p>Privacy and Terms</p>
      <div>
        Made with
        <span role="img" aria-label="heart"> ♥️ </span>
        from Platzi Master at our Community
        <span> ©2020 </span>
      </div>
    </FooterStyled>
  )
};
