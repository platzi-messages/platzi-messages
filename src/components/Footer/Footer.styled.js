import styled from 'styled-components'

const FooterStyled = styled.footer`
  & p {
    margin-top: 4px;
    padding-bottom: 4px;
    font-size: 10px;
    color: ${({ theme }) => theme.colors.border_color};
  }
  & div {
    margin-top: 4px;
    padding-bottom: 4px;
    font-size: 10px;
    color: ${({ theme }) => theme.colors.border_color};
    & span:first-child {
      padding: 0 4px 0 1px;
      font-size: 12px;
      color: ${({ theme }) => theme.colors.actionOne_color};
    }
  }
`

export default FooterStyled
