import styled from 'styled-components'

export const ConversationAreaStyled = styled.main`
  grid-area: main;
  position: relative;
  width: 100%;
  height: 100vh;
  overflow-y: scroll;
  background-color: ${({ theme }) => theme.colors.primary_color};
  &::-webkit-scrollbar {
    display: none;
  }
`
export const MessageArea = styled.div`
  height: 80%;
  overflow-y: scroll;
`
