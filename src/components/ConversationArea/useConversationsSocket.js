
import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'

import useSocket from 'use-socket.io-client'
import { socketEndpoint, socketRoom } from '../../utils/config'

import formatMessage from '../../utils/messagesFunctions'

export default function useConversationsSocket() {
  const location = useLocation()
  const [socket] = useSocket(`${socketEndpoint}`, {
    autoConnect: false,
  })

  const botName = 'Platzi-Message Bot'

  socket.connect()
  socket.emit('message', formatMessage(botName, 'Welcome to Platzi Message!'))

  useEffect(() => {
    socket.emit('join', { username: 'david', room: socketRoom })
    socket.emit('message', { username: 'david', room: socketRoom })

    socket.on('hello', (message) => {
      console.log('message', message)
      // setMessages(
      //   (_messages) => [..._messages, message]
      // )
    })

    socket.on('message', (message) => {
      console.log('message', message)
      // setMessages(
      //   (_messages) => [..._messages, message]
      // )
    })
  }, [location.search, socket])
  return {}
}
