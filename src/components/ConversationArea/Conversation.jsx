import React, { useContext, useEffect, useState } from 'react'
import useSocket from 'use-socket.io-client'
import ChannelMessage from '../ChannelMessage'
import InputMessage from '../InputMessage'
import { ConversationAreaStyled, MessageArea } from './Conversation.styled'
import ConversationHeader from '../ConversationHeader'
import { ConversationsContext } from '../../contexts/ConversationsContext'
import MessagesService from '../../services/messagesService'
import useConversationsSocket from './useConversationsSocket'
import { socketEndpoint, socketRoom } from '../../utils/config'

export default function ConversationArea({ conversationId = 'world' }) {
  const [messages, setMessages] = useState([])
  const [channelId, setChannelId] = useState('')
  const { conversations = {} } = useContext(ConversationsContext)
  const { channels = [] } = conversations
  const [value, setValue] = useState('')
  const isActive = !!channelId
  const [socket] = useSocket(`${socketEndpoint}`, {
    autoConnect: false,
  })
  useConversationsSocket()

  useEffect(() => {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    const channel = channels.find((c) => {
      return c.id === conversationId
    })
    if (channel) {
      setMessages(channel.messages)
      setChannelId(channel.id)
    }
  }, [channels, messages, conversationId, conversations])
  const handleSubmit = (e) => {
    e.preventDefault()
    socket.emit('message', value, socketRoom)
    socket.emit('message', 'Welcome to Platzi Message!')
    console.log(`submitting ${value}`)
    MessagesService.sendMessage(value, channelId)
  }
  return (
    <ConversationAreaStyled onSubmit={handleSubmit}>
      <ConversationHeader />
      <MessageArea>
        {Array.isArray(messages) && messages.map((message) => (
          <ChannelMessage
            key={message.id}
            message={message}
          />
        ))}
      </MessageArea>
      <InputMessage
        channelId={channelId}
        value={value}
        isActive={isActive}
        setValue={setValue}
      />
    </ConversationAreaStyled>
  )
}
