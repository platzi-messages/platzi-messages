import styled from 'styled-components'

export const SideMenuStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: .5em ${(props) => props.theme.space * 2}px;
  font-size: .9em;
  color: ${(props) => props.theme.colors.border_color};
  & h3 {
    font-family: ${(props) => props.theme.fonts.Lato};
  }
`

export const ButtonStyled = styled.button`
  outline: none;
  margin-right: ${(props) => props.theme.space}px;
  border: none;
  background-color: inherit;
  cursor: pointer;
  font-size: 1.3em;
  color: inherit;
  &:hover {
    transform: scale(1.2);
    transition: .5s;
  }
`
