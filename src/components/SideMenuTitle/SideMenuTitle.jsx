import React from 'react'
import { ButtonStyled, SideMenuStyled } from './SideMenuTitle.styled'

export default function SideMenuTitle({ title, Icon, onClick }) {
  return (
    <SideMenuStyled>
      <h3>{title}</h3>
      <ButtonStyled
        aria-label="add channel"
        type="button"
        onClick={onClick}
      >
        <Icon />
      </ButtonStyled>
    </SideMenuStyled>
  )
}
