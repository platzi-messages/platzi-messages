import styled from 'styled-components'

export const ModalStyled = styled.main`
  position: absolute;
  z-index: 2;
  top: 50%;
  left: 50%;
  margin-top: -360px;
  margin-left: -365px;
  padding: ${({ theme }) => theme.space * 2}px;
  width: 40%;
  height: 550px;
  border: 1px solid ${({ theme }) => theme.colors.border_color};
  border-radius: 10px;
  background-color: ${({ theme }) => theme.colors.primary_color};
`

export const Overlay = styled.div`
  position: absolute;
  z-index: 1;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, .4);
`
