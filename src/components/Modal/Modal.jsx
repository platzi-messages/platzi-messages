import React from 'react'
import ReactDOM from 'react-dom'
import { ModalStyled, Overlay } from './Modal.styled'

export default function Modal({ isOpen, children, handleModal }) {
  if (!isOpen) return null

  return (
    ReactDOM.createPortal(
      <>
        <ModalStyled>{children}</ModalStyled>
        <Overlay onClick={handleModal} />
      </>,
      document.querySelector('#modal')
    )
  )
}
