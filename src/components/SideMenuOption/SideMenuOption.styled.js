import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

export const LinkStyled = styled(NavLink)`
display: flex;
padding: .5em ${(props) => props.theme.space * 3}px;
width: 100%;
text-decoration: none;
color: inherit;
& p {
  margin-left: ${(props) => props.theme.space}px;
  font-family: ${(props) => props.theme.fonts.Lato};
}
`

export const ChannelStyled = styled.div`
  width: 100%;
  cursor: pointer;
  font-size: .9em;
  color: ${(props) => (props.has_unreads ? props.theme.colors.text_color : 'grey')};
  &:hover {
    background-color: ${(props) => props.theme.colors.border_color};
  }
`
