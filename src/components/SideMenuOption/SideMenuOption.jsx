import React, { useEffect, useState } from 'react'
import { MdFiberManualRecord, MdHttps } from 'react-icons/md'
import { FiHash } from 'react-icons/fi'
import { ChannelStyled, LinkStyled } from './SideMenuOption.styled'

export default function SideMenuOption({ channel = {}, isDirect = false }) {
  const [channelIcon, setChannelIcon] = useState(<FiHash />)

  useEffect(() => {
    if (channel.is_private) {
      setChannelIcon(<MdHttps />)
    } else {
      setChannelIcon(<FiHash />)
    }
  }, [channel])

  return (
    <ChannelStyled
      has_unreads={channel.has_unreads}
      mention_count={channel.mention_count}
    >
      <LinkStyled
        to={`/${channel.id}`}
        activeStyle={{ backgroundColor: '#33B13A', color: 'white' }}
      >
        { isDirect ? (
          <MdFiberManualRecord
            color={channel.isOnline ? '33B13A' : '9E9EA6'}
          />
        ) : channelIcon}
        <p>{channel.description}</p>
      </LinkStyled>
    </ChannelStyled>
  )
}

