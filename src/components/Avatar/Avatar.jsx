import React, { useContext, useEffect, useState } from 'react'
import { MdPersonOutline } from 'react-icons/md'
import styled from 'styled-components'
import { UsersContext } from '../../contexts/UsersContext'

const AvatarStyled = styled.pre``

export default function Avatar({ idUser }) {
  const { users } = useContext(UsersContext)
  const [user, setUser] = useState({})
  useEffect(() => {
    const usersLocal = Array.isArray(users) ? users.filter((u) => u.id === idUser) : []
    setUser(usersLocal[0] || {
      url_photo: '',
      display_name: ''
    })
  }, [users, idUser])
  return user.url_photo ? (
    <AvatarStyled>
      <img src={user.url_photo} alt={user.display_name} />
    </AvatarStyled>
  ) : <MdPersonOutline />
}
