import React from 'react'
import { Link } from 'react-router-dom'
import Avatar from '../Avatar/Avatar'
import { ChannelMessageStyled, MessageWrapper } from './ChannelMessage.styled'

export default function ChannelMessage({ message }) {
  return (
    <ChannelMessageStyled>
      <Avatar idUser={message.id_user || '31bc08f1-796a-4eb3-a614-4f4a1010f912'} />
      <MessageWrapper>
        <h3><Link to="/">{message.message}</Link></h3>
        <span>{message.ts}</span>
        <p>{message.text}</p>
      </MessageWrapper>
    </ChannelMessageStyled>
  )
}
