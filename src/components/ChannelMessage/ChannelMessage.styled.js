import styled from 'styled-components'

export const ChannelMessageStyled = styled.div`
  display: grid;
  grid-template-columns: 50px 1fr;
  padding: .5em 1em;
  color: ${({ theme }) => theme.colors.text_color};
  &:hover {
    background-color: rgba(0.5, 0.5, 0.5, .2);
  }
  img, svg {
    width: 50px;
    height: 50px;
    border-radius: 10px;
    object-fit: cover;
  }
`
export const MessageWrapper = styled.div`
  margin: 0 1em;
  font-family: ${({ theme }) => theme.fonts.Lato}; 
  text-align: left;
  h3 {
    display: inline-block;
    font-size: 1em;
    &:hover {
      cursor: pointer;
      text-decoration: underline;
    }
  }
  span {
    margin: 0 1em;
    font-size: .9em;
    color: grey;
  }
  p {
    margin: .5em 0;
  }
`
