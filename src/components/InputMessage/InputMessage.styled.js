import styled from 'styled-components'

export const InputMessageStyled = styled.form`
  position: absolute;
  bottom: 0;
  left: 0;
  padding: 0 1em;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.primary_color};
  text-align: left;
  color: white;
`

export const InputWrapper = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
  overflow: hidden;
  border-radius: 5px;
  background-color: ${({ theme, isActive }) => (isActive ? 'white' : theme.colors.border_color)};

  svg {
    width: 40px;
    height: 20px;
    cursor: pointer;
  }
`
export const UserTypingStyled = styled.div`
  margin-top: .5em;
  min-height: 20px;
  color: #9E9EA6;
`

export const InputStyled = styled.input`
  outline: none;
  padding-left: ${({ theme }) => theme.space}px;
  width: 100%;
  height: 40px;
  border: 1px solid #E8E8E8;
  background: transparent;
  font-size: 1em;
`
