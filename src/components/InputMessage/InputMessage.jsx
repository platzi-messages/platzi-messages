import React from 'react'
import { MdAdd, MdInsertEmoticon } from 'react-icons/md'
import { InputWrapper, InputMessageStyled, InputStyled, UserTypingStyled } from './InputMessage.styled'

export default function InputMessage({
  value,
  isActive,
  setValue,
}) {

  return (
    <InputMessageStyled isActive={isActive}>
      <InputWrapper isActive={isActive}>
        <button type="button">
          <MdAdd />
        </button>
        <InputStyled
          value={value}
          placeholder="Escribir un mensaje..."
          disabled={!isActive}
          onChange={(e) => setValue(e.target.value)}
        />
        <button type="button">
          <MdInsertEmoticon />
        </button>
      </InputWrapper>
      <UserTypingStyled>{value && 'User is typing...'}</UserTypingStyled>
    </InputMessageStyled>
  )
}
