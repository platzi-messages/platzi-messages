import React from 'react'
import { StartingMessageStyled } from './StartingMessage.styled'

export default function StartingMessage() {
  return (
    <StartingMessageStyled>
      <img src="/" alt="first message" />
      <h3>Amy rogers</h3>
      <p>You created this channel today. This is the very beginning of the #channelname channel. Purpose: This channel is for team-wide communication and announcements. All team members are in this channel. (edit)</p>
    </StartingMessageStyled>
  )
}
