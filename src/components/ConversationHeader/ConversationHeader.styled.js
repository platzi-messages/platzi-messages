import styled from 'styled-components'

export const ConversationHeaderStyled = styled.div`
  padding: ${({ theme }) => theme.space * 2}px;
  width: 100%;
  border-bottom: 1px solid ${({ theme }) => theme.colors.text_color};
  text-align: left;
  color: ${({ theme }) => theme.colors.text_color};
`

export const Separator = styled.span`
  padding: 0 .5em;
  &::after {
    content: '|'
  }
`

export const HeaderInfo = styled.div`
  padding: .1em;
  button {
    outline: none;
    border: none;
    background-color: inherit;
    cursor: pointer;
    color: ${({ theme }) => theme.colors.text_color};
  }
`

export const HeaderMetadata = styled.div`
  display: flex;
  align-items: center;
  svg {
    font-size: 1.2em;
  }
  button {
    cursor: pointer;
    font-size: 1em;
    color: ${({ theme }) => theme.colors.text_color};
  }
`
