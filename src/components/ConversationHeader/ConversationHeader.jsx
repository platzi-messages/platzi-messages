import React from 'react'
import { MdPersonOutline, MdStarBorder } from 'react-icons/md'
import { ConversationHeaderStyled, HeaderInfo, HeaderMetadata, Separator } from './ConversationHeader.styled'

export default function ConversationHeader({ channelTitle = 'Frontend', count = 560, ChannelTheme = null }) {
  return (
    <ConversationHeaderStyled>
      <HeaderInfo>
        <button type="button">
          <h3>
            #
            {channelTitle}
          </h3>
        </button>
      </HeaderInfo>
      <HeaderMetadata>
        <MdStarBorder />
        <Separator />
        <MdPersonOutline />
        {count}
        <Separator />
        {
          ChannelTheme ? <span>{ChannelTheme}</span> : <button type="button">Añadir un tema</button>
        }
      </HeaderMetadata>
    </ConversationHeaderStyled>
  )
}
