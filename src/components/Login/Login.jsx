import React from 'react'

import { Article, Button, Form } from './Login.styled'
import LogoMessages from '../../assets/images/logoMessages.png'
import LogoGitHub from '../../assets/images/gitHub.png'
import { firebaseApp, githubprovider } from '../../firebase/config'
import useInputValue from '../../hooks/useInputValue'

const Login = () => {
  const [Iemail] = useInputValue('email')
  const [password] = useInputValue('password')

  const handleGithubLogin = () => {
    firebaseApp.auth().signInWithPopup(githubprovider)
      .then((data) => {
        const { profile } = data.additionalUserInfo
        const photo = profile.avatar_url
        const username = profile.login
        const { name } = profile
        const { email } = profile
        const { location } = profile
        const { accessToken } = data.credential
        console.warn(photo, username, name, email, location, accessToken)
      })
  }

  const handleSubmit = () => {
    // Code to submit
  }

  return (
    <Article>
      <figure>
        <img src={LogoMessages} alt="Logo Messages" />
      </figure>
      <h2>Login at Platzi Messages</h2>
      <p>Students Platzi</p>
      <Form aria-label="Login form" onSubmit={handleSubmit}>
        <input
          aria-label="email"
          tabIndex="0"
          autoCorrect="off"
          autoComplete="email"
          type="email"
          {...Iemail}
          placeholder="Write your Email *"
          required
          minLength="4"
          maxLength="50"
        />
        <input
          aria-label="password"
          tabIndex="0"
          autoComplete="current-password"
          type="password"
          {...password}
          placeholder="Write a password"
          minLength="5"
          maxLength="40"
        />
        <Button type="submit">
          Login
        </Button>
      </Form>

      <Button disabled onClick={handleGithubLogin} type="button">
        <img src={LogoGitHub} alt="Logo GitHub" />
        Login With Github
      </Button>
    </Article>
  )
}

export default Login
