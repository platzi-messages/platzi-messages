/* stylelint-disable no-descending-specificity */
import styled from 'styled-components'

export const Article = styled.article`
    display: flex;
    flex-direction: column;
    align-items: center;
    margin: 120px auto 237px;
    width: 272px;
    border: 2px solid ${({ theme }) => theme.colors.border_color};
    border-radius: 10px;
    background-color: ${({ theme }) => theme.colors.primary_color};
    & figure {
        margin-top: 16px;
        width: 128px;
        height: 141;
        & img {
            width: 61.73px;
            height: 68.42px;
        }
    }
    & h2 {
        margin-top: 18px;
        padding: 0;
        box-sizing: border-box;
        font-family: 'Cabin','Helvetica Neue',sans-serif;
        font-size: 18px;
        color: white;
        + p { 
            margin-top: 50px;
            padding: 0;
            font-family: 'Cabin','Helvetica Neue',sans-serif;
            font-size: 12px;
            color: white;
        }
    }

    @media screen and (min-width: ${({ theme }) => theme.sizes.desktop}) {
        width: 530px;
        height: 600px;

        & figure img {
            width: 120px;
            height: 130px;
        }
        & h2 {
            margin-top: 40px;
            font-size: 24px;
            + p {
                margin-top: 100px;
                font-size: 15px;
            }
        }
    }
`

export const Form = styled.form`
    margin: 10px 0;
    & input {
        display: block;
        outline: none;
        margin-bottom: 10px;
        padding: 10px;
        width: 100%;
        border: 1px solid white;
        border-radius: 6px;
        background: none;
        color: white;
    }
`

export const Button = styled.button`
    position: relative;
    display: flex;
    flex-direction: center;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
    margin-bottom: 10px;
    padding: 8px;
    width: 200px;
    border: none;
    border-radius: 6px;
    background-color: green;
    cursor: pointer;
    font-size: 15px;
    color: white;
    & img {
        position: absolute;
        left: 15px;
        width: 12px;
        height: 12px;
    }
    & p {
        margin-left: 30px;
        padding: 0;
        font-size: 11px;
        color: white;
    }
    &:disabled {
        opacity: 0.7;
        cursor: default;
    }

    @media screen and (min-width: ${({ theme }) => theme.sizes.desktop}) {
        margin-top: 20px;
        width: 304px;
        height: 36px;
        & img {
            width: 26px;
            height: 26px;
        }
        & p {
            margin-left: 60px;
            font-size: 15px;
        }
    }
`
