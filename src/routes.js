import Main from './pages/Main'
import LoginScreen from './pages/LoginScreen'
import ROUTES from './utils/routes'

const routes = [
  {
    layout: ROUTES.LOGIN,
    path: '/',
    title: 'Login',
    component: LoginScreen,
    exact: true,
  },
  {
    layout: ROUTES.MAIN,
    path: '/:id',
    title: 'Conversation',
    component: Main,
    exact: true,
  },
  {
    layout: ROUTES.MAIN,
    path: '/', // remember '/' should be at the end of the routes (because every route uses the '/')
    title: 'Platzi Messages',
    component: Main,
    exact: true,
  },
]

export default routes
