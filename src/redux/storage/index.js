import { combineReducers, createStore } from 'redux'
import reducer from '../reducers'

const AllReducers = combineReducers({
  list: reducer,
})

export const store = createStore(AllReducers)

export default {}
