/**
 * Action example
 */
const example = (props) => {
  return {
    type: 'EXAMPLE',
    payload: props,
  }
}

export default example
