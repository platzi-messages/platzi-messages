
const initialState = {
  value: 0
}

const example = (state = initialState, action) => {
  switch (action.type) {
    default:
      return state
  }
}

export default example
