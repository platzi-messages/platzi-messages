import React, { useState } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import { ThemeProvider } from 'styled-components'

import StyledApp from './AppStyles'
import themes from './styled/themes/index'
import GlobalStyles from './styled/GlobalStyles'
import routes from './routes'
import Layout from './components/Layout'
import ContextComponents from './contexts/ContextComponents'

function App() {
  const [theme, setTheme] = useState(themes.light)

  const toggleTheme = () => setTheme(theme === themes.light ? themes.dark : themes.light)

  const getPath = (route) => `${route.layout !== '/' ? route.layout : ''}${route.path}`

  if (false) toggleTheme() // Temp conditional to solve no-unused-vars <== Delete

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <GlobalStyles />
        <ContextComponents>
          <StyledApp>
            <Layout>
              <Switch>
                {
                  routes.map((route) => {
                    const path = getPath(route)
                    return (
                      <Route
                        path={path}
                        exact={route.exact}
                        key={`route${route.layout}${route.path}`}
                        component={route.component}
                      />
                    )
                  })
                }
              </Switch>
            </Layout>
          </StyledApp>
        </ContextComponents>
      </Router>
    </ThemeProvider>
  )
}

export default App
