import React from 'react'
import Login from '../components/Login/Login'
import Footer from '../components/Footer/Footer'

const LoginScreen = () => {
  return (
    <>
      <Login />
      <Footer />
    </>
  )
}

export default LoginScreen
