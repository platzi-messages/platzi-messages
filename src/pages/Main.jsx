import React, { useContext, useState } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { useParams } from 'react-router-dom'
import ConversationArea from '../components/ConversationArea/index'
import Modal from '../components/Modal/Modal'
import Profile from '../components/Profile/Profile'
import SideMenu from '../components/SideMenu'
import { ConversationsContext } from '../contexts/ConversationsContext'

const MainStyled = styled.div`
  display: grid;
  grid-template-columns: 300px 1fr;
  grid-template-areas: "side main";
`
const mapStateToProps = ({ messages }) => {
  return {
    messages
  }
}

function Main() {
  const { conversations } = useContext(ConversationsContext)
  const [openModal, setOpenModal] = useState(false)
  if (false) setOpenModal()
  const { id = 'C86LK55U0' } = useParams()

  return (
    <MainStyled>
      <SideMenu conversations={conversations} />
      <ConversationArea conversationId={id} />
      <Modal isOpen={openModal} handleModal={() => setOpenModal(!openModal)}>
        <Profile />
      </Modal>
    </MainStyled>
  )
}
export default connect(mapStateToProps, null)(Main)
