export const ENV = process.env.REACT_APP_ENV
export const ENVIRONMENTS = {
  local: 'local',
  development: 'development',
  staging: 'staging',
  production: 'production',
}
export const HOTJAR_ID = process.env.REACT_APP_HOTJAR_ID
export const HOTJAR_VERSION = process.env.REACT_APP_HOTJAR_VERSION
export const socketEndpoint = process.env.REACT_APP_SOCKET_ENDPOINT || 'ws://localhost:4007'
export const apiEndpoint = process.env.REACT_APP_API_ENDPOINT || 'http://localhost:4001'
export const socketRoom = process.env.REACT_APP_SOCKET_ROOM || '123'
