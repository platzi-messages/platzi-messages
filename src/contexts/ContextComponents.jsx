import React from 'react'
import { ConversationsContext, useFetchInitialConversations } from './ConversationsContext'
import { MessagesContext, useFetchInitialMessages } from './MessagesContext'
import { useFetchInitialUsers, UsersContext } from './UsersContext'

export default function ContextComponents({ children }) {
  const { messages, setMessages } = useFetchInitialMessages()
  const { users, setUsers } = useFetchInitialUsers()
  const { conversations, setConversations, addMessage } = useFetchInitialConversations()
  return (
    <UsersContext.Provider value={{ users, setUsers }}>
      <MessagesContext.Provider value={{ messages, setMessages }}>
        <ConversationsContext.Provider value={{ conversations, setConversations, addMessage }}>
          {children}
        </ConversationsContext.Provider>
      </MessagesContext.Provider>
    </UsersContext.Provider>
  )
}
