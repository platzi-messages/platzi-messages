import React, { useEffect, useState } from 'react'
import UsersService from '../services/userService'

export const UsersContext = React.createContext([])

export const useFetchInitialUsers = () => {
  const [users, setUsers] = useState([])
  useEffect(() => {
    UsersService.get()
      .then((u) => {
        setUsers(u)
      })
  }, [])
  return { users, setUsers }
}
