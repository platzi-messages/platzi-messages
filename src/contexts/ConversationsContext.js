import React, { useEffect, useState } from 'react'
import ConversationsService from '../services/conversationsService'

export const ConversationsContext = React.createContext([])

export const useFetchInitialConversations = () => {

  const [conversations, setConversations] = useState([])
  const fetchConversations = async () => {
    const conversationsResponse = await ConversationsService.get()
    setConversations(conversationsResponse)
  }

  useEffect(() => {
    fetchConversations()
  }, [])

  const addMessage = (idChannel, message) => {
    console.log('addMessage -> message', message)
    console.log('addMessage -> idChannel', idChannel)
  //   const { channels } = conversations
  //   const newChannels = channels.map((c) => {
  //     if (c.id === idChannel) {
  //       return { ...c, messages: [...c.messages, message] }
  //     }
  //     return c
  //   })
  //   setConversations(newChannels)
  }
  return { addMessage, conversations, setConversations }

}
