import React, { useEffect, useState } from 'react'
import MessagesService from '../services/messagesService'

export const MessagesContext = React.createContext([])

export const useFetchInitialMessages = () => {
  const [messages, setMessages] = useState([])
  useEffect(() => {
    MessagesService.getAll()
      .then((_messages) => {
        setMessages(_messages)
      })
  }, [])
  return { messages, setMessages }
}
