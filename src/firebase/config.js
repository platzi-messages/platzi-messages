import firebase from 'firebase/app'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyCV-h6C-PSBqNTCTll1xf3Kdmy7wkKQoiI',
  authDomain: 'platzi-messages.firebaseapp.com',
  databaseURL: 'https://platzi-messages.firebaseio.com',
  projectId: 'platzi-messages',
  storageBucket: 'platzi-messages.appspot.com',
  messagingSenderId: '844725636163',
  appId: '1:844725636163:web:f8a86ef2f487df2b6518f6',
  measurementId: 'G-V9HZ36FT3Z'
}
// Initialize Firebase
export const firebaseApp = firebase.initializeApp(firebaseConfig)

export const githubprovider = new firebase.auth.GithubAuthProvider()
