import { createGlobalStyle } from 'styled-components'

const GlobalStyles = createGlobalStyle`
  * {
    margin:0;
    padding:0;
    box-sizing: border-box;
    font-family: 'Cabin', 'Helvetica Neue', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body {
    margin: 0;
    background: #1F2B49;
    font-size: 1em;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
    monospace;
  }

  button {
    outline: none;
    border: none;
    background-color: inherit;
  }

  #root {
    margin: auto;
    max-width: 1920px;
  }

  div {
    background-color: #1F2B49; 
  }

  a {
    text-decoration: none;
    color: inherit;
  }
`

export default GlobalStyles
