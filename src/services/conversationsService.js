import { apiEndpoint, ENV, ENVIRONMENTS } from '../utils/config'
import conversationsMock from '../mocks/conversations.mock.json'
import MessagesService from './messagesService'

export default class ConversationsService {
  static async get() {
    let data
    const messages = await MessagesService.getAll()
    try {
      if (ENV !== ENVIRONMENTS.local) {
        const url = `${apiEndpoint}/channels`
        const response = await fetch(url)
        const json = await response.json()
        const { body } = json
        const channels = body.map((channel) => {
          const channelMessages = messages.filter((message) => message.id_channel === channel.id)
          return { ...channel, messages: channelMessages }
        })
        data = { channels }
      } else {
        data = conversationsMock
      }
    } catch (err) {
      data = { 'ok': false }
      console.error(err)
    }
    return data
  }
}
