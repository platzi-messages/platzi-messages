import { ENV, ENVIRONMENTS, apiEndpoint } from '../utils/config'
import messagesMock from '../mocks/messages.mock.json'

export default class MessagesService {
  static async getAll() {
    let data = {}
    try {
      if (ENV !== ENVIRONMENTS.local) {
        const url = `${apiEndpoint}/messages`
        const response = await fetch(url)
        const json = await response.json()
        data = json.body
      } else {
        data = messagesMock
      }
    } catch (e) {
      console.error(e)
    }
    return data
  }

  static async sendMessage(
    message,
    channelId,
    userId = '31bc08f1-796a-4eb3-a614-4f4a1010f912'
  ) {
    let data = {}
    try {
      if (ENV !== ENVIRONMENTS.local) {
        const url = `${apiEndpoint}/messages`
        const body = {
          message,
          id_user: userId,
          id_channel: channelId,
          type: 'message'
        }
        const requestOptions = {
          method: 'POST',
          redirect: 'follow',
          referrerPolicy: 'no-referrer',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body)
        }
        const response = await fetch(url, requestOptions)
        const json = await response.json()
        data = json.body
      } else {
        data = messagesMock
      }
    } catch (e) {
      console.error(e)
    }
    return data
  }
}
