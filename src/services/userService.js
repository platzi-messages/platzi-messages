import { ENV, ENVIRONMENTS, apiEndpoint } from '../utils/config'

export default class UsersService {
  static async get() {
    let data = {}
    try {
      if (ENV !== ENVIRONMENTS.local) {
        const url = `${apiEndpoint}/users`
        const response = await fetch(url)
        const json = await response.json()
        data = json.body
      } else {
      }
    } catch (e) {
      console.error(e)
    }
    return data
  }
}
